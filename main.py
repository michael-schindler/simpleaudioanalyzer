import os

import logging

import numpy as np
from scipy.signal import chirp
from scipy.io import wavfile
import sounddevice as sd

import matplotlib.pyplot as plt


def run_test(name):
    # Use a breakpoint in the code line below to debug your script.
    # Press Strg+8 to toggle the breakpoint.

    T = 10.  # /home/michael/PycharmProjects
    fs = 48.0e3
    t = np.linspace(0., T, int(T*fs))
    f_limits = [20.0, 20.0e3]
    y = chirp(t, f_limits[0], T, f_limits[1], method='logarithmic', phi=90.)

    sd.default.samplerate = int(fs)
    sd.default.device = [6, 6]

    print(sd.query_devices())
    # print(sd.check_input_settings(device=20, channels=[1], samplerate=int(fs)))
    # print(sd.check_input_settings(device=6, channels=[1], samplerate=int(fs)))

    # sd.play(y, fs)
    y_rec = sd.playrec(y, fs, channels=1)
    sd.wait()

    wavfile.write(os.path.join('.', 'mysignal.wav'), int(fs), y)
    wavfile.write(os.path.join('.', 'myrecording.wav'), int(fs), y_rec)
    f = np.arange(t.size) / t.size * fs
    y_fft = np.fft.fft(y)
    y_rec_fft = np.fft.fft(y_rec)

    [fig, [ax_amplitude, ax_phase]] = plt.subplots(2, 1, sharex='col', figsize=(10, 6))
    lines = []
    lines += ax_amplitude.plot(f, 20*np.log10(np.abs(y_rec_fft)), label='measurement')
    lines += ax_amplitude.plot(f, 20*np.log10(np.abs(y_fft/np.amax(np.abs(y_fft)))), label='signal')

    lines += ax_phase.plot(f, np.angle(y_rec_fft))
    lines += ax_phase.plot(f, np.angle(y_fft))

    ax_amplitude.grid(which='both', axis='both')
    ax_amplitude.set_xscale('log')
    ax_phase.grid(which='both', axis='both')
    ax_phase.set_xscale('log')

    ax_amplitude.legend()
    ax_amplitude.set_ylabel('Amplitude |A(f)| in dB')
    ax_amplitude.set_xlim((f_limits[0], fs/2))

    ax_phase.set_ylabel('Phase in rad')
    ax_phase.set_xlabel('f in Hz')

    fig.savefig(os.path.join('.', 'myrecording.png'), format='png')
    plt.show()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    run_test('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
